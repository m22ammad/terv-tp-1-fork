using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CreateOnClick : MonoBehaviour
{
    public GameObject aCopier;
    public Color col;
    public InputAction activateInstantiation;
    // Start is called before the first frame update
    void Start()
    {
        activateInstantiation.Enable(); // on active l'action
        activateInstantiation.performed += changeProperty; // on instantie, change la couleur, place devant player et applique une force
    }



    private void changeProperty(InputAction.CallbackContext context)
    {
        GameObject obj = Instantiate(aCopier); // on copie l'objet à copier (surlequel il y a un clic droit)
        obj.GetComponent<Renderer>().material.color = Color.yellow; // on change la couleur
        obj.transform.position = Camera.main.transform.position + Camera.main.transform.forward*8 ;
        obj.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * 20000);
        

    }

        // Update is called once per frame
        void Update()
    {
        
    }
}
