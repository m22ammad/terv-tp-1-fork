using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RotateCube : MonoBehaviour
{
    public float speed = 3.0f;
    int i ;
    public InputAction activateRotation;
    // Start is called before the first frame update
    void Start()
    {
        speed = 0;
        activateRotation.Enable();
        activateRotation.performed += status;

    }
    private void status(InputAction.CallbackContext context){
        {
            if (i % 2 == 0)
            {
                speed = 0;
                Debug.Log("Vous avez appuyé sur r : arrêt");
            }
            else
            {
                speed = 3;
                Debug.Log("Vous avez appuyé sur r : tourne");
            }

            i++;
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        //transform.RotateAround(Vector3.up, 3);
        transform.Rotate(0,speed,0);


    }
}
